  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard v2</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Alumnos</a></li>
              <li class="breadcrumb-item active">Editar</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Alumnos</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <?php

             /* esto iria dentro del FORM?
              if(isset ($_POST ['apellido'])){
                  $apellido = $_POST['apellido']

                  $campo = array();

                  if($apellido == ""){
                    array_push ($campo, "tiene que agregar un apellido");
                  }
              }*/
                  try {
                    $stmt = "SELECT * FROM usuarios WHERE ID = $id";
                    $resultado = $conn->query($stmt);
                    
                } catch (Exception $e) {
                    $error = $e->getMessage();
                    echo $error;
                }
                $student = $resultado->fetch_assoc();
                  /*die(var_dump($student));*/
              ?>
              <form action="includes/models/studentsModel.php" id="guardar-registro" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Nombre</label>
                    <input type="text" class="form-control" id="exampleInputName" placeholder="Nombre del Alumno" name="name" value="<?php echo $student['nombre']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDni">Apellido</label>
                    <input type="text" class="form-control" id="exampleInputDni" placeholder="Apellido del Alumno" name="lastname" value="<?php echo $student['apellido']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputLastname">DNI</label>
                    <input type="number" class="form-control" id="exampleInputLastname" placeholder="DNI del Alumno" name="dni" value="<?php echo $student['DNI']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="email" value="<?php echo $student['email']; ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword" placeholder="Enter password" name="password" value="<?php echo $student['pass']; ?>">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                <input type="hidden" name="registro" value="actualizar">
                <input type="hidden" name="id_registro" value="<?php echo $id; ?>">
                <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
