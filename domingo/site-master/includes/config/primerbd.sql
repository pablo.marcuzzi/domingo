-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-11-2020 a las 18:49:43
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `primerbd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnosmaterias`
--

CREATE TABLE `alumnosmaterias` (
  `ID_alumno` int(11) NOT NULL,
  `ID` int(11) NOT NULL,
  `ID_materias` int(11) NOT NULL,
  `Rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE `carreras` (
  `ID_carreras` int(11) NOT NULL,
  `nombreCarrera` varchar(50) NOT NULL,
  `plandeEstudio` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE `materias` (
  `ID_materias` int(11) NOT NULL,
  `ID_carrera` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `cargaHoraria` int(11) NOT NULL,
  `correlatividad` int(11) NOT NULL,
  `Profesor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ID` int(11) NOT NULL,
  `apellidoNombre` varchar(50) NOT NULL,
  `Rol` int(11) NOT NULL,
  `DNI` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` int(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `alumnosmaterias`
--
ALTER TABLE `alumnosmaterias`
  ADD PRIMARY KEY (`ID_alumno`),
  ADD KEY `ID` (`ID`,`ID_materias`,`Rol`),
  ADD KEY `ID_materias` (`ID_materias`);

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD PRIMARY KEY (`ID_carreras`),
  ADD KEY `nombreCarrera` (`nombreCarrera`),
  ADD KEY `plandeEstudio` (`plandeEstudio`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`ID_materias`),
  ADD KEY `Profesor` (`Profesor`),
  ADD KEY `Nombre` (`Nombre`,`cargaHoraria`,`correlatividad`),
  ADD KEY `cargaHoraria` (`cargaHoraria`,`correlatividad`),
  ADD KEY `cargaHoraria_2` (`cargaHoraria`),
  ADD KEY `cargaHoraria_3` (`cargaHoraria`),
  ADD KEY `ID_carrera` (`ID_carrera`),
  ADD KEY `Nombre_2` (`Nombre`,`cargaHoraria`,`correlatividad`,`Profesor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `apellidoNombre` (`apellidoNombre`),
  ADD KEY `apellidoNombre_2` (`apellidoNombre`,`Rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `alumnosmaterias`
--
ALTER TABLE `alumnosmaterias`
  MODIFY `ID_alumno` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `carreras`
--
ALTER TABLE `carreras`
  MODIFY `ID_carreras` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `materias`
--
ALTER TABLE `materias`
  MODIFY `ID_materias` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumnosmaterias`
--
ALTER TABLE `alumnosmaterias`
  ADD CONSTRAINT `alumnosmaterias_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `usuarios` (`ID`) ON UPDATE CASCADE,
  ADD CONSTRAINT `alumnosmaterias_ibfk_2` FOREIGN KEY (`ID_materias`) REFERENCES `materias` (`ID_materias`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `carreras`
--
ALTER TABLE `carreras`
  ADD CONSTRAINT `carreras_ibfk_1` FOREIGN KEY (`ID_carreras`) REFERENCES `materias` (`ID_materias`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `materias`
--
ALTER TABLE `materias`
  ADD CONSTRAINT `materias_ibfk_1` FOREIGN KEY (`ID_materias`) REFERENCES `carreras` (`ID_carreras`) ON UPDATE CASCADE,
  ADD CONSTRAINT `materias_ibfk_2` FOREIGN KEY (`Nombre`) REFERENCES `carreras` (`nombreCarrera`) ON UPDATE CASCADE,
  ADD CONSTRAINT `materias_ibfk_3` FOREIGN KEY (`ID_carrera`) REFERENCES `carreras` (`ID_carreras`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `alumnosmaterias` (`ID_alumno`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
