  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard v2</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Usuarios</a></li>
              <li class="breadcrumb-item active">Nuevo</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Usuarios</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="includes/models/studentsModel.php" id="guardar-registro" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputName">Apellido/s</label>
                    <input type="text" class="form-control" id="exampleInputName" placeholder="Apellido del Alumno" name="apellido">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDni">Nombre</label>
                    <input type="text" class="form-control" id="exampleInputApellido" placeholder="Nombre del Alumno" name="nombre">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputDni">Rol</label>
                     
                       <label for="">Rol</label>
                        <select required name = "usuario ">
                          
                          <option value="admin">Administrador</option>
                          <option value="alumno" selected >Alumnos</option>
                          <option value="profesor">Profesores</option>
                        </select><br>
                    
                    <input type="text" class="form-control" id="exampleInputRol" placeholder="Selecciones un rol de la lista" name="rol">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputLastname">DNI</label>
                    <input type="number" class="form-control" id="exampleInputLastname" placeholder="D.N.I. del Alumno" name="dni">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Ingrese email" name="email">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword" placeholder="Ingrese Password" name="pass">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                <input type="hidden" name="registro" value="nuevo">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    </aside>
  <!-- /.control-sidebar -->
