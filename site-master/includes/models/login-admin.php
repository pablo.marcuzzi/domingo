<?php
//con la funcion isset verificamos si esta creada la variable post->login-admin
if(isset($_POST['login-admin'])) {
    // si existe asignamos los valores que vienen por post email y password en las variables usuario y password
    $usuario = $_POST['email'];
    $password = $_POST['pass'];

    try {
        include_once '../config/bd_conexion.php';
        // realizamos la consulta a la base de datos en la tabla students y filtramos por los datos que coincidan con email
        $stmt = $conn->prepare("SELECT * FROM usuarios WHERE email = ?;");
	    $stmt->bind_param('s', $usuario);
	    $stmt->execute();
        $stmt->bind_result($id, $apellido, $nombre, $DNI, $Rol, $email, $pass, $dateCreate);
        if($stmt->affected_rows) {
            // Si la consulta arroja un resultado los datos lo guardamos en el array existe
            $existe = $stmt->fetch();
            // Si existe tiene datos
            if($existe) {
                //realizamos la validacion de que el password que viene del formulario y el password guardado en la base de datos sean iguales
                if(password_verify($password, $pass)) {
                    //Iniciamos la session
                    session_start();
                    // Seteamos nuestras variables de secion con los datos que vienen de la base de datos
                    $_SESSION['email'] = $email;
                    $_SESSION['apellido'] = $apellido
                    $_SESSION['nombre'] = $nombre;
                    $_SESSION['rol'] = $Rol;
                    $_SESSION['id'] = $id;
                    $respuesta = array(
                        'respuesta' => 'exitoso',
                        'usuario' => $nombre
                    );
                } else {
                    $respuesta = array(
                    'respuesta' => 'error_pass',
                    );
                }
            } else {
                $respuesta = array(
                    'respuesta' => 'error',
                    'email' => $usuario
                    
                );
            }
        }
        $stmt->close();
        $conn->close();
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
    die(json_encode($respuesta));
}
